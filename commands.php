<?php
date_default_timezone_set('America/Los_Angeles');

// ubuntu:winningteam 10.0.0.119 start date is 2014-06-02
// I have 12 cores total, 10 parallel threads work fine, 12 do not
// each batch of 10 threads finishes in 8 min, 300 min (5 hours)

$path   = '/mnt/fileshare/CentralNic/xyz/registry-escrow/';
$before = new DateTime('today');
$after  = new DateTime('yesterday');
while($before >= new DateTime("2016-01-01"))
{
    $before = $path.$before->format('Y-m-d')."/R0.domains.csv.gz";
    $after  = $path.$after->format('Y-m-d')."/R0.domains.csv.gz";
    $output = "~/summaries/".$after->format('Y-m-d').".csv";
    
    echo "php ~/compare.php ".$before." ".$after." > ".$output."\n";

    $before->sub(new DateInterval('P1D'));
    $after->sub(new DateInterval('P1D'));
}


// php commands.php > commands.txt
// parallel -j+0 < commands.txt

// mysql --local-infile -uroot -p xyz
// LOAD DATA LOCAL INFILE "file.csv" INTO TABLE changes FIELDS TERMINATED BY ',' LINES TERMINATED BY '\n' IGNORE 1 LINES (domain,registrar,status,date);
