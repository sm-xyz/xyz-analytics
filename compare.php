<?php
// this line must go first
ini_set("auto_detect_line_endings", true);

// compare.php before.csv after.csv
list($name, $before_file, $after_file) = $argv;


if(!file_exists($before_file))
    exit("Before file does not exist\n");

if(!file_exists($after_file))
    exit("After file does not exist\n");


// gzopen should work for regular files too
$fp_before     = gzopen($before_file, "r");
$fp_after      = gzopen($after_file, "r");

$titles_before = fgetcsv($fp_before);
$titles_after  = fgetcsv($fp_after);

if($titles_before != $titles_after)
    exit("Both files must contain the same column titles\n");


// I need to distinguish between a domain that has expired and the one that has renewed. Both will need to be recorded.
// Either expiration date is in the past or the domain is not there, both count as an expiration.
// So to record the result, I will need the following: domain, registrar, status (expired/renewed), date 
// I will need to have an index on (date, status)
// Note that the domain can be renewed multiple times, so all three things will have to be unique



echo "domain,registrar,status,date\n";

$advance     = 'both';
$line_before = $line_after = "";
$row_before  = $row_after  = [];
do
{
    // it looks like the data is sorted by id, which matches roid
    // advance the line pointer on the smaller of the two ids

    // some authinfo columns contain a backslash, which screws up the parsing, hence using '"' as the escape character
    if($advance == 'both' || $advance == 'before')
    {
        $line_before = trim(fgets($fp_before));
        if($line_before)
        {
            $cells_before = str_getcsv($line_before, ',', '"', '"');
            array_splice($cells_before, array_search('authInfo', $titles_before), -1, ['authInfo']);
            $row_before   = array_combine($titles_before, $cells_before);
        }

        //if(count($titles_before) !== count($cells_before))
        //    echo "Something fishy at this before line: ".print_r($cells_before, 1)." = ".$line_before."\n";
    }

    if($advance == 'both' || $advance == 'after')
    {
        $line_after = trim(fgets($fp_after));
        if($line_after)
        {
            $cells_after = str_getcsv($line_after, ',', '"', '"');
            array_splice($cells_after, array_search('authInfo', $titles_after), -1, ['authInfo']);
            $row_after   = array_combine($titles_after, $cells_after);
        }

        //if(count($titles_after) !== count($cells_after))
        //    echo "Something fishy at this before line: ".print_r($cells_after, 1)." = ".$line_after."\n";
    }


    // two variables, four possible outcomes (null, <, ==, >)
    if(!$line_before || !$line_after)
    {
        //null : null     => we reached the end of both files, the number of records stayed the same
        //null : not null => we reached the end of the before file first, new records were added
        //not null : null => we reached the end of the after file first, old records were deleted

        //echo "reached the end of one of the files.\n";
        if(!$line_before && $line_after)
        {
            //echo $row_after['id']." is new\n";
            $advance = 'after';
        }
        elseif($line_before && !$line_after)
        {
            $advance   = 'before';

            $domain    = $row_before['name'];
            $registrar = $row_before['clID'];
            $status    = 'expired';
            $date      = basename(pathinfo($after_file, PATHINFO_DIRNAME));

            echo "$domain,$registrar,$status,$date\n";
        }
        //else echo "reached the end of both files\n";
    }
    else
    {
        //before < after  => after skipped an id that used to be there, it got deleted
        //before == after => both ids are present, but other properties may have changed
        //before > after  => before skipped an id, it got added

        if($row_before['id'] < $row_after['id'])
        {
            $advance   = 'before';

            $domain    = $row_before['name'];
            $registrar = $row_before['clID'];
            $status    = 'expired';
            $date      = basename(pathinfo($after_file, PATHINFO_DIRNAME));

            echo "$domain,$registrar,$status,$date\n";
        }
        elseif($row_before['id'] > $row_after['id'])
        {
            //echo $row_after['id']." is new\n";
            $advance = 'after';
        }
        else
        {
            if($row_before != $row_after)
            {
                //echo $row_before['id']." has changed\n";
                $changes = get_changes($row_before, $row_after);
                //print_r($changes);

                // expiration date change change by itself, without updating the status
                if(isset($changes['exDate']))
                {
                    $ex_date_before = new DateTime($changes['exDate'][0]);
                    $ex_date_after  = new DateTime($changes['exDate'][1]);

                    // name, clID
                    if($ex_date_after > $ex_date_before)
                    {
                        $domain     = $row_after['name'];
                        $registrar  = $row_after['clID'];
                        $status     = 'renewed';
                        $date       = basename(pathinfo($after_file, PATHINFO_DIRNAME));

                        echo "$domain,$registrar,$status,$date\n";
                        //echo $row_before['name']." renewed: ".$changes['exDate'][0]." -> ".$changes['exDate'][1]."\n";
                    }
                }
            }

            $advance = 'both';
        }
    }
}
while($line_before || $line_after); // keep reading as long as one of the files has more lines to read




function get_changes($a, $b)
{
    $changes = [];
    foreach($a as $key => $value)
    {
        if($value != $b[$key])
            $changes[$key] = [$value, $b[$key]];
    }
    return $changes;
}